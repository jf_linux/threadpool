#include "threadpool_t.h"
#include <iostream>
std::mutex __mutex;
void printf(void *data_)
{
    __mutex.lock();
	//std::this_thread::sleep_for(std::chrono::seconds(1));
    std::cout<<"data:"<<(char*)data_<<std::endl;
    __mutex.unlock();
}
int main()
{
	char buf[] = "threadpool2";
	threadpool_t threadpool;
	
	threadpool.init(100,1);
    std::cout<<"threadpool start ..."<<std::endl;

	for (uint32_t i = 0 ;i < 100; ++i)
	{
		task_t task(printf,buf);
		threadpool.add_task(task);
	}
	std::this_thread::sleep_for(std::chrono::seconds(1));
	threadpool.destroy();
	return 0;
}